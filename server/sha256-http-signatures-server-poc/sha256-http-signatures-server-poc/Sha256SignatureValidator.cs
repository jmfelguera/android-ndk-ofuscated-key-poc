using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Nancy;
using Nancy.Diagnostics;
using Nancy.Extensions;
using Nancy.IO;

namespace sha256_http_signatures_server_poc
{

    public interface HttpSignatureValidator
    {
        bool Validate(Request request);
        string CalculateHash(Request request); 
        string CalculateHash(Response response);
        Response AddHashToHeader(Response response);
    }
    public class Sha256SignatureValidator : HttpSignatureValidator
    {
        protected readonly String privateKey;
        protected readonly byte[] privateKeyBytes;
        protected readonly String headerName;
        protected readonly  bool enforce;
        protected readonly bool allowFailures;
        protected HMACSHA256 hmac;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="privateKey"></param>
        /// <param name="enforce">If true, it will return false if signature header is not present. Default: false</param>
        /// <param name="allowFailures">If true it will return valid responses when an exception happens within the code. Default:false</param>
        public Sha256SignatureValidator(String privateKey, String headerName, bool enforce = false, bool allowFailures=false)
        {
            this.privateKey = privateKey;
            this.headerName = headerName;
            this.enforce = enforce;
            this.allowFailures = allowFailures;
            this.privateKeyBytes = generateKey(privateKey);
            this.hmac = new HMACSHA256(this.privateKeyBytes);
        }
        public bool Validate(Request request)
        {
            // header not present, if enforce is true, then return false
            var header = request.Headers.FirstOrDefault(x =>headerName.Equals(x.Key,StringComparison.InvariantCultureIgnoreCase));
            if (header.Key == null)
            {
                return !enforce;
            }
            //header is present, calculate hash of the request body and compare it with the 
            // one present in the headers 
            var requestHash = CalculateHash(request);
            return String.Join("",header.Value).Trim() == requestHash;
        }

        public string CalculateHash(Request request)
        {
            var data = request.Url.ToString() + request.Body.AsString();
            return calculateHash(data);
        }

        public string CalculateHash(Response response)
        {
            throw new NotImplementedException();
        }

        public Response AddHashToHeader(Response response)
        {
            var hash = calculateHash(readResponseBody(response));
            response.Headers.Add(headerName,hash);
            return response;
        }

        protected String calculateHash(string data)
        {
            var computeHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
            return Convert.ToBase64String(computeHash);
        }

        private static byte[] generateKey(String key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return Encoding.UTF8.GetBytes(key);
        }
        
        private static String readResponseBody(Response response)
        {
            var currentContents = response.Contents;
            using (var msr = new MemoryStream())
            {
                currentContents(msr);
                using (var reader = new StreamReader(msr))
                {
                    reader.BaseStream.Seek(0, SeekOrigin.Begin);
                    return reader.ReadToEnd();
                }
            }
        }

    }
    
}