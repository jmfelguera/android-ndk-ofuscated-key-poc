using System;
using System.IO;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using Nancy.TinyIoc;

namespace sha256_http_signatures_server_poc
{
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        private readonly HttpSignatureValidator signatureValidator;
        public CustomBootstrapper()
        {
            this.signatureValidator = new Sha256SignatureValidator("clavemolona", "X-GS-Signature");
        }
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            pipelines.BeforeRequest += (ctx) =>
            {
                if (signatureValidator.Validate(ctx.Request))
                {
                    // valid, continue with the execution of the request
                    return null;
                }
                else
                {
                    return new TextResponse(HttpStatusCode.Unauthorized, "Invalid Signature");

                }
            };

            pipelines.AfterRequest += ctx =>
            {
                signatureValidator.AddHashToHeader(ctx.Response);
            };
        }
    }
}