﻿using System;
using Nancy;
using Nancy.Hosting.Self;

namespace sha256_http_signatures_server_poc
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var hostconfiguration = new HostConfiguration();
            hostconfiguration.RewriteLocalhost = true;
            var apiHost = new NancyHost(hostconfiguration, new Uri("http://localhost:8778"));
            Console.WriteLine("Server started on port 8778....launch requests to /test path ;)");
            Console.WriteLine("Pulse Enter to stop the server ");
            Console.WriteLine("Running");
            
            apiHost.Start();
            Console.ReadLine();
            apiHost.Stop();
        }
    }

    public class TestModule : NancyModule
    {
        public TestModule()
        {
            Get["/test"] = _ => "Esto es una prueba";
        }
    }
}