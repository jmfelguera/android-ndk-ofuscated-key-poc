package com.example.test

import okio.ByteString.Companion.encodeUtf8
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


class Sha256Signature {
    companion object {
        fun calculateSignature(payload: String): String {
            val sha256Hmac = Mac.getInstance("HmacSHA256")
            val secretKey =
                SecretKeySpec(Secrets().getZZQOCPRM("com.example.test").toByteArray(Charsets.UTF_8), "HmacSHA256")
            sha256Hmac.init(secretKey)

            val computehash = sha256Hmac.doFinal(payload.toByteArray(Charsets.UTF_8))

            return Base64.getEncoder().encodeToString(computehash)
        }

        fun isValidSignature(signature: String, payload: String): Boolean {
            return signature == calculateSignature(payload)
        }
    }
}