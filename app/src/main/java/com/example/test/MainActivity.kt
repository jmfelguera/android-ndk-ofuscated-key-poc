package com.example.test

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okio.ByteString.Companion.encodeUtf8
import java.io.IOException
import java.nio.charset.Charset


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val potcbpvv = Secrets().getZZQOCPRM("com.example.test")
        Log.i("CLAVECHULA", "La clave es: $potcbpvv")

        val client = OkHttpClient()
            .newBuilder()
            .addInterceptor(customInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()

        val request: Request = Request.Builder()
            .url("http://192.168.96.137:8778/test")
            .build()

        client.newCall(request).enqueue(object : Callback {
            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                Log.i("SUCCESS", response.body.toString())
            }

            override fun onFailure(call: Call, e: IOException) {
                Log.e("FAIL!!", e.localizedMessage, e)
            }
        })
    }

    private val customInterceptor = Interceptor { chain ->
        Log.i("INTERCEPTOR", "Request Intercepted")

        val signedRequest = signRequest(chain.request())
        val response = chain.proceed(signedRequest)

        Log.i("INTERCEPTOR", "Response Intercepted")
        validateResponse(response)

        response
    }

    private fun validateResponse(response: Response) {
        val signatureValue = response.headers("X-GS-Signature").first()

        if (signatureValue.isNotEmpty())
            if (!Sha256Signature.isValidSignature(signatureValue, readResponseBody(response)))
                throw RuntimeException("Bad Response, somebody is playing to spies")
    }

    private fun signRequest(request: Request): Request {
        val url = request.url.toUrl().toString()
        val sha256Signature = Sha256Signature.calculateSignature(url)

        return request.newBuilder()
            .addHeader("X-GS-Signature", sha256Signature)
            .build()
    }

    private fun readResponseBody(response: Response): String {
        val source = response.body!!.source()
        source.request(Long.MAX_VALUE) // Buffer the entire body.
        val buffer = source.buffer
        return buffer.clone().readString(Charset.forName("UTF-8"))
    }

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}